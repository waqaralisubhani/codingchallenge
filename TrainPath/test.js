const TrainGraph = require('./index');

test('Train Graph is a class', () => {
  expect(typeof TrainGraph.prototype.constructor).toEqual('function');
});

test('can add vertex to a graph', () => {
    const graph = new TrainGraph();
    expect(() => {
      graph.addVertex("A");
    }).not.toThrow();
  });

test('can add edge to a graph', () => {
    const graph = new TrainGraph();
    graph.addVertex("A")
    graph.addVertex("B")
    
    expect(() => {
      graph.addEdge("A", "B", 4);
    }).not.toThrow();
  });
  
  test('can detect shortest path', () => {
    const graph = new TrainGraph();
    graph.addVertex("A");
    graph.addVertex("B");
    graph.addVertex("C");
    graph.addVertex("D");
    graph.addVertex("E");
    graph.addVertex("F");
    graph.addEdge("A", "B", 4);
    graph.addEdge("A", "C", 2);
    graph.addEdge("B", "E", 3);
    graph.addEdge("C", "D", 2);
    graph.addEdge("C", "F", 4);
    graph.addEdge("D", "E", 3);
    graph.addEdge("D", "F", 1);
    graph.addEdge("E", "F", 1);
    let pathObj = graph.shortestPathFinder('A', 'E');
    let {path, distances} = pathObj;
    const {distance} = graph.retrieveDistance(path, distances);
    expect(distance).toBe(7);
    expect(path).toStrictEqual(['A', 'B', 'E']);
    let newPath = graph.shortestPathFinder('A', 'M');
    const calculatedDistance = graph.retrieveDistance(newPath.path, newPath.distances)
    expect(calculatedDistance).toBe(null);
  });
