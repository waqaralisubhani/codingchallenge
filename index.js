
const TrainGraph = require('./TrainPath');
const parse = require('csv-parse')
const fs = require('fs')
const readline = require("readline")

const args = process.argv;
let fileName = '';

fileName = args[2] && fs.existsSync(args[2]) ? args[2] : 'routes.csv';


const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("what station are you getting on the Train?", function(pathA) {
    rl.question("what station are you getting off the Train", function(pathB) {
        calculateRoute(pathA, pathB)
        rl.close();
    });
});

const csvData=[];
const graph = new TrainGraph();

function populateData(csvData) {
    for (let i = 0; i < csvData.length; i++) {
        const element = csvData[i];
        graph.addVertex(element[0]);
        graph.addVertex(element[1]);
        graph.addEdge(element[0], element[1], parseInt(element[2]));
    }
}

function calculateRoute(pathA, pathB) {
    // reading the csv file 
    fs.createReadStream(fileName)
        .pipe(parse({delimiter: ','}))
        .on('data', function(csvrow) {
            csvData.push(csvrow);        
        })
        .on('end',function() {
        //populate csv data
        populateData(csvData)
        let {path, distances} = graph.shortestPathFinder(pathA, pathB);
        let calculatedDistance = graph.retrieveDistance(path, distances);
        if (calculatedDistance === null) {
            console.log(`No routes from ${pathA} to ${pathB}`)
        } else {
            const {stops, distance} = calculatedDistance;
            console.log(`${stops} stops, ${distance} minutes`)
        }
        
        });

}
