# Requirements

node.js version (11.10 or latest stable recommendend )

## Commands

#### install the npm packages
npm install or yarn install

#### Run the project
npm run start  or yarn start 

### You can pass file as an argument 
npm run start filepath/filename
e.g 
npm run start file/test.csv

#### run the test cases
yarn test or npm run test

#### run the coverage with view on web
yarn coverage or npm run coverage


